﻿/*******************************************************************************
 * Copyright © 2020 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/
using QuartzUI.Code;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using QuartzUI.Service.SystemOrganize;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using QuartzUI.Domain;
using System.Collections.Generic;

namespace QuartzUI.Web.Controllers
{
	[HandlerLogin]
    public class ClientsDataController : Controller
    {
        public UserService _userService { get; set; }
        /// <summary>
        /// 获取当前用户信息请求方法
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAjaxOnly]
        [AllowAnonymous]
        public async Task<ActionResult> GetUserCode()
        {
            var currentuser = _userService.currentuser;
            if (currentuser.UserId==null)
            {
                return Content("");
            }
            var data =await _userService.GetFormExtend(currentuser.UserId);
            return Content(data.ToJson());
        }
        /// <summary>
        /// 初始菜单列表请求方法
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetInitDataJson()
        {
            var currentuser = _userService.currentuser;
            if (currentuser.UserId == null)
            {
                return Content("");
            }
            var roleId = currentuser.RoleId;
            StringBuilder sbJson = new StringBuilder();
            InitEntity init = new InitEntity();
            init.homeInfo = new HomeInfoEntity();
            init.homeInfo.href = GlobalContext.SystemConfig.HomePage;
            init.logoInfo = new LogoInfoEntity();
            //修改主页及logo参数
            init.logoInfo.title = "QuartzUI";
            init.logoInfo.image = "../icon/favicon.ico";
            init.menuInfo = new List<MenuInfoEntity>();
            MenuInfoEntity munu = new MenuInfoEntity();
            munu.title = "定时任务";
            munu.target = "_self";
            munu.icon= "fa fa-home";
            munu.child = new List<MenuInfoEntity>();
            MenuInfoEntity munutemp = new MenuInfoEntity();
            munutemp.title = "定时任务";
            munutemp.target = "_self";
            munutemp.href = GlobalContext.SystemConfig.HomePage;
            munutemp.child = new List<MenuInfoEntity>();
            munu.child.Add(munutemp);
            init.menuInfo.Add(munu);
            sbJson.Append(init.ToJson());
            return Content(sbJson.ToString());
        }
    }
}
