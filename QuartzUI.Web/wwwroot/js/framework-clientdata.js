﻿var currentUser = {};
$(function () {
    if (!!top.currentUser && top.currentUser.hasOwnProperty("Id")) {
        currentUser = top.currentUser;
    }
    else {
        currentUser = $.userInit();
    }
})
$.userInit = function () {
    var dataJson = {};
    var init = function () {
        $.ajax({
            url: "/ClientsData/GetUserCode?v=" + new Date().Format("yyyy-MM-dd hh:mm:ss"),
            type: "get",
            dataType: "json",
            async: false,
            success: function (data) {
                dataJson = data;
            }
        });
    }
    init();
    return dataJson;
}