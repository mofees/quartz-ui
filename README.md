<p></p>
<p></p>

<p align="center">
<img src="https://images.gitee.com/uploads/images/2021/0528/215200_accc4311_7353672.jpeg" height="80"/>
</p>
<div align="center">

![](https://img.shields.io/badge/.net-6.0.0-blue)
![](https://img.shields.io/badge/SqlSugar-5.0.8.6-blue)
![](https://img.shields.io/badge/layui-2.6.12-blue)
</div>

----
# WaterCloud

#### 介绍

- 请勿用于违反我国法律的项目上。
- 定时任务，基于quartz的定时任务功能(可以集群)。
- 目前支持api、job、sql

#### 环境要求

1. VS2022及以上版本；
2. Asp.net 6.0；

#### 友情链接

1. 前端框架Layui 文档地址：https://layui.gitee.io/v2/
2. Layui前端框架Layuimini 码云地址：https://gitee.com/zhongshaofa/layuimini
3. SqlSugar.ORM 文档地址：https://www.donet5.com/home/doc
4. WaterCloud讨论交流QQ群（1065447456）[![](https://pub.idqqimg.com/wpa/images/group.png)](https://jq.qq.com/?_wv=1027&k=51RHQVG)
5. .NET易用底层框架 Furion，码云地址：https://gitee.com/dotnetchina/Furion

#### 捐赠支持

开源项目不易，若此项目能得到你的青睐，可以捐赠支持作者持续开发与维护，感谢所有支持开源的朋友。


![输入图片说明](https://images.gitee.com/uploads/images/2020/0331/144842_7cf04ad6_7353672.jpeg "1585637076201.jpg")          ![输入图片说明](https://images.gitee.com/uploads/images/2020/0331/144852_8b26c8cb_7353672.png "mm_facetoface_collect_qrcode_1585637044089.png")