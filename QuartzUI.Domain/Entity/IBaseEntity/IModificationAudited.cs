﻿/*******************************************************************************
 * Copyright © 2020 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/
using System;

namespace QuartzUI.Domain
{
    public interface IModificationAudited
    {
        string Id { get; set; }
        string LastModifyUserId { get; set; }
        DateTime? LastModifyTime { get; set; }
    }
}