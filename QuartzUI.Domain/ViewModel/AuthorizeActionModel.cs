﻿/*******************************************************************************
 * Copyright © 2020 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartzUI.Domain.ViewModel
{
    public class AuthorizeActionModel
    {
        public string Id { set; get; }
        public string UrlAddress { set; get; }
        public string Authorize { get; set; }
    }
}
