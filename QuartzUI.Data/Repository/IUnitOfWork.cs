﻿/*******************************************************************************
 * Copyright © 2020 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/
using SqlSugar;

namespace QuartzUI.DataBase
{
	public interface IUnitOfWork
	{
		SqlSugarClient GetDbClient();
		void BeginTrans();

		void Commit();
		void Rollback();
		void CurrentBeginTrans();

		void CurrentCommit();
		void CurrentRollback();
	}
}